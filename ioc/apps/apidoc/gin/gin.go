package gin

import (
	"gitee.com/go-kade/library/v2/ioc/config/gogin"
	"gitee.com/go-kade/library/v2/ioc/config/http"
	"github.com/gin-gonic/gin"
	"github.com/swaggo/swag"
)

func (h *SwaggerApiDoc) Registry() {
	r := gogin.InitRouter(h)
	r.GET("/", func(c *gin.Context) {
		c.Writer.WriteString(swag.GetSwagger(h.InstanceName).ReadDoc())
	})

	h.log.Info().Msgf("Get the API Doc using http://%s", http.Get().ApiObjectAddr(h))
}
