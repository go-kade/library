package metric

import (
	"gitee.com/go-kade/library/v2/ioc"
	"gitee.com/go-kade/library/v2/ioc/apps/metric"
	"gitee.com/go-kade/library/v2/ioc/config/log"
	"github.com/rs/zerolog"
)

func init() {
	ioc.Api().Registry(&restfulHandler{
		Metric: metric.NewDefaultMetric(),
	})
}

type restfulHandler struct {
	log *zerolog.Logger
	ioc.ObjectImpl

	*metric.Metric
}

func (h *restfulHandler) Init() error {
	h.log = log.Sub(metric.AppName)
	h.Registry()
	return nil
}

func (h *restfulHandler) Name() string {
	return metric.AppName
}

func (h *restfulHandler) Version() string {
	return "v1"
}

func (h *restfulHandler) Meta() ioc.ObjectMeta {
	meta := ioc.DefaultObjectMeta()
	meta.CustomPathPrefix = h.Endpoint
	return meta
}
