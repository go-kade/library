package grpc

import "gitee.com/go-kade/library/v2/ioc"

const (
	AppName = "grpc"
)

func Get() *Grpc {
	return ioc.Config().Get(AppName).(*Grpc)
}
