package application

import "gitee.com/go-kade/library/v2/ioc"

const (
	AppName = "app"
)

func Get() *Application {
	return ioc.Config().Get(AppName).(*Application)
}
