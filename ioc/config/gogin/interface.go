package gogin

import (
	"gitee.com/go-kade/library/v2/ioc"
	"gitee.com/go-kade/library/v2/ioc/config/http"
	"github.com/gin-gonic/gin"
)

const (
	AppName = "gin_webframework"
)

func RootRouter() *gin.Engine {
	return ioc.Config().Get(AppName).(*GinFramework).Engine
}

// 添加对象前缀路径
func InitRouter(obj ioc.Object) gin.IRouter {
	modulePath := http.Get().ApiObjectPathPrefix(obj)
	return ioc.Config().Get(AppName).(*GinFramework).Engine.Group(modulePath)
}
