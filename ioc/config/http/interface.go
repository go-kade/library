package http

import "gitee.com/go-kade/library/v2/ioc"

const (
	AppName = "http"
)

func Get() *Http {
	obj := ioc.Config().Get(AppName)
	if obj == nil {
		return defaultConfig
	}
	return obj.(*Http)
}
